import sys
import socket
import ssl

MSG = "usage: python3 SecuMid/server.py <listen_addr> <listen_port> <server_cert> <server_key> <client_cert>"

def main(args):
    if len(args) != 5:
        print(MSG)
        return 1
    
    listen_addr = args[0]
    listen_port = int(args[1])
    server_cert = args[2]
    server_key = args[3]
    client_cert = args[4]
    
    context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_cert_chain(certfile=server_cert, keyfile=server_key)
    context.load_verify_locations(client_cert)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind((listen_addr, listen_port))
        sock.listen(5)
        with context.wrap_socket(sock, server_side=True) as ssock:
            conn, addr = ssock.accept()
            print("connection from: {}".format(addr))
            while True:
                data = conn.recv(1024)
                print("received: {}".format(data))
                conn.sendall(b'received')
                if data == b'quit':
                    print("server shutting down")
                    break
    
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
# SecuMid-py
Maintainer: Alexandru Iulian Orhean (alexandru.orhean@gmail.com)  

SecuMid (Secure Middleware) is a cross-platform secured implementation of a message-based client-server middleware 
(in Python 3) that uses TLS/SSL and Google Protocol Buffers to transfer messages between the client and the server.

## Setup

Before you can run the middleware you will need to generate certificates for the server and for the client. The server
and the client can reside on different machines connected through a network. To generate the certificates and the key 
for one of the machine you can run:
```
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout machine.key -out machine.crt
```

## How to Run

You will need to start the server first:
```
python3 SecuMid/server.py <listen_addr> <listen_port> <server_cert> <server_key> <client_cert>
```

And then you can start the client:
```
python3 SecuMid/client.py <server_addr> <server_port> <client_cert> <client_key> <server_cert>
```

import sys
import socket
import ssl

MSG = "usage: python3 SecuMid/client.py <server_addr> <server_port> <client_cert> <client_key> <server_cert>"

def main(args):
    if len(args) != 5:
        print(MSG)
        return 1
    
    server_addr = args[0]
    server_port = int(args[1])
    client_cert = args[2]
    client_key = args[3]
    server_cert = args[4]

    context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=server_cert)
    context.load_cert_chain(certfile=client_cert, keyfile=client_key)

    with socket.create_connection((server_addr, server_port)) as sock:
        with context.wrap_socket(sock, server_hostname=server_addr) as ssock:
            ssock.sendall(b'hello world (secured)')
            data = ssock.recv(1024)
            if data != b'received':
                print("error")
            
            ssock.sendall(b'are you alive?')
            data = ssock.recv(1024)
            if data != b'received':
                print("error")

            ssock.sendall(b'quit')
            data = ssock.recv(1024)
            if data != b'received':
                print("error")

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
